% completa risoluzione di un sistema lineare usando la riduzione
% in matrice triangolare superiore con gauss pivoting parziale
% e algoritmo di bottom up con sostituzione


U=[2,4,-2;1,-1,5;4,1,-2] % matrice di input
b=[6;-3;-10] %termini noti

[x,y] = convert_matrix_to_triangular_matrix_gauss_naif(U,b)
[x1] = linear_system_resolver_upper_triangular_matrix(x,y)

