eig(a)   autovalori della matrice a

qr(a) fattorizzazione matric a in q*r con q ortogonale(inversa e trasposta uguale) e r triangolare superiore
    2/3 n 3 operazioni.
    
eningenvalues_qr(a, iterazioni,tolleranza) ccalcola tuttgli gli autovalori usando qr
    se a ha autovalori reali la matrice converge a triangolare superiore
        e si richiede che abs(a1) > abs(a2) > abs(a3) ...
        la velocita con cui gli elementi sotto la diagonale convergono a 0 e' bassa se gli autovalori  sono molto vicini tra loro    
    se a ha autovalori complessi la matrice converge a quasi triangolare superiore con elmenti non zero sotto la diagonale

    extra:
            Teorema: A reale simmetrica, esiste Q ortogonale t.c.
        D = QTA Q diagonale
        (la trasformazione non avviene in un numero finito di passi)

        Teorema: A reale, esiste Q ortogonale t.c. QTA Q � di
        2
        Hessenberg (tridiagonale se A � simmetrica)
        (la trasformazione avviene in un numero finito di passi)

    
power_method funziona solo se il massimo in valore assoluto degli autovalori di una matrice e' unico
 e' lento se gli autovalori  sono molto vicini tra loro