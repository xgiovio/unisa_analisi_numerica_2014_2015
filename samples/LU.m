
%se A � la matrice originale
% e A1 la matrice dopo la riduzione di gauss naif
% allora A = L * U

L=tril(A1,-1) + eye (size(A1));
U=triu(A1);

A = L * U;





%se A � la matrice originale
% e A1 la matrice dopo la riduzione di gauss pivot
% allora P A = L * U

L=tril(A1,-1) + eye (size(A1));
U=triu(A1);
% P matrice di permutazione in base al pivot 

P * A = L * U;



%comando matlab per fattorizzare A
[L,U,P] = lu(A);



