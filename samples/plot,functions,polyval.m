f=@(x) x^2 * cos(x)   usa ./ .* .^ per divisioni moltiplicazioni elevazioni elemento per elemento
fplot(f, [-4 4],'r*' )
hold on

x=1:0.1:10  oppure x=linspace(-5,5,100) 100 punti tra -5,5
y=f(x)

p= polyfit(x,y,n) // n � il grado, 1 in meno dei punti
p= [1,2,3,4,5] //coefficinti del polinomio

xx=1:0.01:10 
yy=polyval(p,xx)
plot(xx,yy)


