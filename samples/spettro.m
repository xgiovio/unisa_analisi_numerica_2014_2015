%Ax=b
%A = X + Y

%x = inv(X) *b - (inv(X) * Y *x)

% se C = inv(X) * b    e     B = -(inv(X) * Y )
% allora x = Bx + C

%metodo iterativo converge se e solo se il raggio spettrale di B < 1
% raggio spettrale = max (abs(eig(B)))


%in jacobi X = diag(diag(A))  e Y = A - X
%in gauss-seidel X = tril(A)  e Y = A - X



%quindi

%jacobi B =  -(inv(diag(diag(A))) * (A - (diag(diag(A)))) )
%jacobi C =  inv(diag(diag(A))) * b

%gauss B =  -(inv(tril(A)) * (A - (tril(A))) )
%gauss C =  inv(tril(A)) * b 


%formula definitiva
%jacobi converge se
% max(abs(eig((-(inv(diag(diag(A))) * (A - (diag(diag(A)))) )))))
% <1

%gauss converge se
% max(abs(eig(-(inv(tril(A)) * (A - (tril(A))) ))))
% <1





