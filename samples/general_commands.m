diag (diag(...)) (estrae una matrice diagonale)
tril(...) (estrae il triangolo inferiore)
inv(...) (inversa di una matrice)
max (...) (massimo di un vettore)
abs(...) (valore assoluto di un vettore componenteper componente)
eig(...) (autovalori di una matrice)
cond(...) (indice di condizionamento di una matri
chol(...) (fattorizzazione di Cholesky di una matrice
lu(...) (fattorizzazione lu di una matrice)
norm (norma di un vettore o di una matrice)
' (trasposta di una matrice)
. (operazioni componente per componente)
help (fornisce informazioni sui comandi in linea di matlab
clear all (cancella tutte le variabili)
clc 