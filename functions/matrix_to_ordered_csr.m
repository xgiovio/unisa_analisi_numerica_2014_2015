function [array_elementi,array_colonna, array_inizio_riga ] = matrix_to_ordered_csr( A )
%conversione matrice in formato ordinato csr usando 3 array


sizem = size(A);

nelemento = 1;

array_elementi=[];
array_colonna=[];
array_inizio_riga=[];


for j=1:sizem(1)
    array_inizio_riga(j)= nelemento;
    for i=1:sizem(2)
        if A(j,i)~= 0
            array_elementi( nelemento)= A(j,i);
            array_colonna ( nelemento) = i;
            nelemento=nelemento + 1;
        end
    end
end
 array_inizio_riga(j +1 )= nelemento;

end

