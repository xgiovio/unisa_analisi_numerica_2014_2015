
% questo algoritmo prende in input una matrice tridiagonale
% e i termini noti : ax=b e ne calcola la soluzione x
% usando il metodo di thomas lineare

%U=[10,20,30;1,5,3;4,56,34];  matrice di input
%b=[1;2;17]; termini noti

function [x] = thomas_tridiagonal_matrix_no_pivoting (U,b)


    n= length(b); % lunghezza del vettore dei termini noti o numero equazioni
    norma = norm(U,inf);

    ok=1;
           % convertiamo la matrice 
            for i=1:1:n-1
                
                if abs(U(i,i)) <= eps * norma
                    disp('WARNING: Elemento prossimo allo zero sulla diagonale')
                end
                
                if U(i,i) == 0
                      ok=0;
                      break

                end
                j=i+1;
                m= U(j,i) / U(i,i);
                U(j,i+1) =  U(j,i+1) + ( U(i,i+1) * (- m ) ) ;
                U(j,i)=m;

            end

            if ok == 0 || U(n,n) == 0
                disp('impossibile convertire')
                x=[];
            else
                if abs(U(n,n)) <= eps * norma
                    disp('WARNING: Elemento prossimo allo zero sulla diagonale')
                end
 
                Low=tril(U,-1) + eye (size(U));
                Upp=triu(U);
               
                %1 ly=b
                %2 ux=y
                
                y = linear_system_resolver_lower_triangular_matrix(Low,b);
                x = linear_system_resolver_upper_triangular_matrix(Upp,y);
                
            end
    
end
