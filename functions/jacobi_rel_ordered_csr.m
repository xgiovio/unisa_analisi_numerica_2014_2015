
% jacobi algorithm

%input
%elementi,colonne,indice_riga=ordered csr
%b=[1;2;17] termini noti
%x0 vettore di partenza
%toll tolleranza relativa , esempio 1e-4
%nmax numero massimo di passaggi, esempio 500


%Ooutput
%x0 soluzione computata
%err errore relativo computato
%niter numero di interazioni eseguite
%ier 1 se il numero di iterazioni ha raggiunto nmax, 0 altrimenti

function [x0,err,niter,ier] = jacobi_rel_ordered_csr (elementi,colonne,indice_riga,b,x0,toll,nmax)

    n= length(b); % lunghezza del vettore dei termini noti o numero equazioni
    niter = 0;
    ier = 0;
    err = inf;% err infinito per superare subito toll
    x1 = x0;% solo per inizializzare
    
    while ( niter < nmax ) && ( err >= toll)
        for i=1:n
            partial_sum = 0;
            partial_sum2 = 0;

            start = indice_riga(i);
            stop = start;
            while colonne(stop)~= i
                stop = stop +1;
            end
            for k=start:(stop -1)
                partial_sum = partial_sum + (elementi(k) * x0(colonne(k)));
            end
            for k=(stop +1):(indice_riga(i+1) - 1)
                partial_sum2 = partial_sum2 + (elementi(k) * x0(colonne(k)));
            end
            x1(i) = (b(i) - partial_sum - partial_sum2)/ elementi(stop) ;
        end
        niter = niter + 1;
        err = norm(x1 - x0,inf) / norm (x1,inf);
        x0=x1;
        mex =  [' Iterazione ', num2str(niter),' : ',mat2str(x0), ' Errore relativo : ',  num2str(err)];
        disp (mex)
    end
    if niter == nmax
        disp('Warning: Massimo numero di step raggiunti')
        ier = 1;
    end
 
end
