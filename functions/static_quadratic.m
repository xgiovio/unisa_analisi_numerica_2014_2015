function [ areai,areaf,err,m ] = static_quadratic( f,m )
%algoritmo dei trapezi per approssimare un integrale
%   input
%   f = function handle
%   m = matrice n x 2 ordinata in modo crescente per righe rispetto alla
%   prima colonna. la prima colonna rappresenta un punto x, la seconda
%   colonna la f(x). ogni valore della seconda colonna deve essere uguale a
%   0 o essere la f(x) del punto x della prima colonna sulla medesima riga.
%   output
%   areai = area iniziale
%   areaf = area finale
%   err = errore area iniziale - finale in valore assoluto
%   m = la matrice iniziale con i nuovi punti aggiunti


[x,~] = size(m);
for i=1:x
    if m(i,2) == 0
        m(i,2)= f(m(i,1));
    end
end

areai=0;
for i=1:x-1
    k=i+1;
    areai= areai + (((m(i,2) + m(k,2)) * ( m(k,1) - m(i,1) )) /2);  
end

i=1;
k=i+1;
j=x;
while i<= ((x * 2)-2)
    valuex= ( m(i,1) + m(k,1) ) /2;
    valuey= f(valuex);
    m= [m(1:i,:);valuex,valuey;m(k:j,:)];
    i=i+2;
    k=k+2;
    j= j + 1;
end

areaf=0;
for i=1:j-1
    k=i+1;
    areaf= areaf + (((m(i,2) + m(k,2)) * ( m(k,1) - m(i,1) )) /2);  
end

err = abs(areaf - areai) / 3;
end

