
% questo algoritmo converte una matrice quadrata generica in
% triangolare superiore mediante metodo di gauss
% i moltiplicatori sono salvati nella matrice inferiore

%U=[10,20,30;1,5,3;4,56,34];  matrice di input
%b=[1;2;17]; termini noti

function [U,b] = convert_matrix_to_triangular_matrix_gauss_naif (U,b)


    n= length(b); % lunghezza del vettore dei termini noti o numero equazioni
    norma = norm(U,inf);
    U= [U,b]; % uniamo la matrice con la colonna dei termini noti

    
    ok=1;
           % convertiamo la matrice
            for i=1:1:n-1
                
                if abs(U(i,i)) <= eps * norma
                    disp('WARNING: Elemento prossimo allo zero sulla diagonale')
                end
                
                if U(i,i) == 0
                      ok=0;
                      break

                end
                   for j=i+1:1:n
                        m= U(j,i) / U(i,i);
                        U(j,i+1:n+1) =  U(j,i+1:n+1) + ( U(i,i+1:n+1) * (- m ) ) ; 
                        U(j,i)=m;
                   end
                
            end

            if ok == 0 || U(n,n) == 0
                disp('impossibile convertire')
                b=[];
                U=[];
            else
                if abs(U(n,n)) <= eps * norma
                    disp('WARNING: Elemento prossimo allo zero sulla diagonale')
                end
                b=U(:,n+1);
                U=U(:,1:n);
            end
    
end
