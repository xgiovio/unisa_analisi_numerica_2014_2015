

function [vn,a] = power_method (U,toll)

[x,~] = size(U);
for i=1:x
    v(i)=1;
end
v=v';
vn=v;

err = inf;
while err > toll
    v1=U*v;
    v1n=v1./norm(v1,inf);
    err = norm(v1n-vn,inf);
    a = v1./v;
    v=v1;
    vn=v1n;
end


 
end
