function [ area, err ] = integratore_schema_fisso( f, m, toll )
%algoritmo dei trapezi composto a schema fisso
%   input
%   f = function handle
%   m = matrice n x 2 ordinata in modo crescente per righe rispetto alla
%   prima colonna. la prima colonna rappresenta un punto x, la seconda
%   colonna la f(x). ogni valore della seconda colonna deve essere uguale a
%   0 o essere la f(x) del punto x della prima colonna sulla medesima riga.
%   toll = tolleranza assoluta voluta
%   output
%   area = area finale
%   err = errore assoluto

    err = inf;
    while (err > toll)
        [~,area,err,m] = static_quadratic(f,m);
    end
    
    
    [j,~] = size(m); 
    hold on
    fplot (f,[m(1,1) m(j,1)],'k')
    plot (m(:,1),m(:,2),'*r')
    hold off
    
end

