%conversione matrice quadrata a banda in versione compatta

function [A] = compact_band_quadratic_matrix (U,left,right)

dim = size(U);
dimy= dim(2);
A = zeros(left + right + 1,dimy);

%upper
x=1;Ax=right;
for y=2:(right + 1)
    
    Uy=y;
    for Ux=x:(dimy - (y - 1))
        
        A(Ax,Uy) = U(Ux,Uy);
      
        Uy=Uy+1;
    end
    
    
     Ax=Ax-1; 
end

%lower
y=1;Ax=right + 2 ;
for x = 2:(left +1)
    
    Ux=x;
    for Uy=y: (dimy - (x- 1))
       A(Ax,Uy)=U(Ux,Uy); 
      
       Ux=Ux+1;
    end
    
   Ax=Ax+1; 
end

%diagonal
for x=1:dimy
    A(right+1,x) = U(x,x);
    
end
