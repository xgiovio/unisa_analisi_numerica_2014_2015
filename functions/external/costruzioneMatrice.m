function A=costruzioneMatrice(G,p)
% c = out-degree, r = in-degree
[n,n] = size(G);
c = sum(G,1);
r = sum(G,2);

% Scale column sums to be 1 (or 0 where there are no out links).

k = find(c~=0);
D = sparse(k,k,1./c(k),n,n);

% Solve (I - p*G*D)*x = e

e = ones(n,1);
I = speye(n,n);

%Conventional power method
   z = ((1-p)*(c~=0) + (c==0))/n;
   A = p*G*D + e*z;