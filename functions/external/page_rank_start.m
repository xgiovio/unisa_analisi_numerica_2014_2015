load unisa100.mat 
% si possono generare mediante 
%[U,G] = surfer('http://www.xgiovio.com',100);
p = 0.85;
A=costruzioneMatrice(G,p)

toll=1e-5;
x=power_method(A,toll);

% Grafico a barre del page rank.

shg
bar(x)
title('Page Rank')

% Stampa degli URLs in ordine di page rank.

[ignore,q] = sort(-x);
disp('     page-rank  url')
k = 1;
n=length(x)
while (k <= n) & (x(q(k)) >= .005)
    j = q(k);
    disp(sprintf(' %3.0f %8.4f %s', ...
        j,x(j),U{j}))
    k = k+1;
end

