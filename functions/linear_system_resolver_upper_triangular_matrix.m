%risoluzione matrice triangolare superiore con algoritmo
% di sostituzione botton up (all'indietro)

%example input
% U=[2,2,4;0,-7,-11;0,0,2];
% b=[5;-8;-2];


function[x] = linear_system_resolver_upper_triangular_matrix(U,b)

    x=[];
    n= length(b);
    x(n) = b(n)/U(n,n);

    for i=n-1:-1:1
        somma = 0;
        for k=i+1:n
            somma=somma+U(i,k) * x(k);
        end

        x(i) = (b(i) - somma)/ U(i,i);
    end
end