function [ area, err, ier ] = integratore_adattivo_locale_extended( f, m, toll , maxite )
%algoritmo dei trapezi composto, addattivo con controllo di errore locale
%a differenza del locale questo algoritmo continua la suddivisone della
%curva, utilizzando il minor numero possibile di punti,
%una volta raggiunto maxite per fornire l'errore dei punti piu a
%destra
%   input
%   f = function handle
%   m = matrice n x 2 ordinata in modo crescente per righe rispetto alla
%   prima colonna. la prima colonna rappresenta un punto x, la seconda
%   colonna la f(x). ogni valore della seconda colonna deve essere uguale a
%   0 o essere la f(x) del punto x della prima colonna sulla medesima riga.
%   toll = tolleranza assoluta voluta
%   maxite = numero massimo di iterazioni
%   output
%   area = area finale
%   err = errore assoluto
%   ier = indice di errore

minlen_factor=1e-6;
[x,~] = size(m);   
i=1;
k=i+1;
j=x;
minlen=(m(j,1) - m(i,1)) * minlen_factor ;
ite = 0;
area=0;
err = 0;
ier = 0;
    while (i <= j - 1)
        ltoll = (toll * (m(k,1) - m(i,1))) / (m(j,1) - m(i,1)); 
        [~,larea,lerr,m1] = static_quadratic(f,m(i:k,:));
        m=[m(1:i-1,:);m1;m(k+1:j,:)];
        ite = ite + 1;
        j= j + 1;
        while (lerr > ltoll) && (ite < maxite) &&( (m(j,1) - m(i,1)) > minlen )
            ltoll = (toll * (m(k,1) - m(i,1))) / (m(j,1) - m(i,1)); 
            [~,larea,lerr,m1] = static_quadratic(f,m(i:k,:));
            m=[m(1:i-1,:);m1;m(k+1:j,:)];
            ite = ite + 1;
            j= j + 1;     
        end
         if (m(j,1) - m(i,1)) <= minlen
            disp('Warning: Minlen raggiunto')
         end
        area = area + larea;
        err = err + lerr;
        
        i = i + 2;
        k = k + 2;
    end
    if ite >= maxite
        disp('Warning: Massimo numero di step raggiunti')
        ier = 1;
    end
    
      
    hold on
    fplot (f,[m(1,1) m(j,1)],'k')
    plot (m(:,1),m(:,2),'*r')
    hold off
    
end

