
% jacobi algorithm

%input
%U= matrice di input compattata
%b=[1;2;17]; termini noti
%x0 vettore di partenza
%toll tolleranza relativa , esempio 1e-4
%nmax numero massimo di passaggi, esempio 500


%Ooutput
%x0 soluzione computata
%err errore relativo computato
%niter numero di interazioni eseguite
%ier 1 se il numero di iterazioni ha raggiunto nmax, 0 altrimenti

function [x0,err,niter,ier] = jacobi_rel_compact (U,b,x0,toll,nmax)

    n= length(b); % lunghezza del vettore dei termini noti o numero equazioni
    niter = 0;
    ier = 0;
    err = inf;% err infinito per superare subito toll
    x1 = x0;% solo per inizializzare
    
    %detect right and left
    x=1;
    y=1;
    left= 0;
    right= 0;
    while U(x,y)==0
        right = right + 1;
        x=x+1;
    end
    dim= size(U);
    dimx=dim(1);
    left = dimx  - right - 1;
    
    %begin iterations
    
    while ( niter < nmax ) && ( err >= toll)
        for i=1:n
            partial_sum = 0;
            partial_sum2 = 0;

            kend = min (left, i - 1);
            for k=1:kend
                partial_sum = partial_sum + ( U(right +1 +k,i-k) * x0(i-k));
            end
            
            kend = min (right, n - i);
            for k=1: kend
                partial_sum2 = partial_sum2 + ( U(right+1-k,i+k) * x0(i+k));
            end
            x1(i) = (b(i) - partial_sum - partial_sum2)/U(right +1,i) ;
        end
        niter = niter + 1;
        err = norm(x1 - x0,inf) / norm (x1,inf);
        x0=x1;
        mex =  [' Iterazione ', num2str(niter),' : ',mat2str(x0), ' Errore relativo : ',  num2str(err)];
        disp (mex)
    end
    if niter == nmax
        disp('Warning: Massimo numero di step raggiunti')
        ier = 1;
    end
 
end
