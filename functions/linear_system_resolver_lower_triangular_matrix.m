%risoluzione matrice triangolare inferiore con algoritmo
% di sostituzione botton up (all'indietro)

%example input
% U=[1,0,0;1,1,0;1,5,1];
% b=[5;-8;-2];


function[x] = linear_system_resolver_lower_triangular_matrix(U,b)

    x=[];
    n= length(b);
    x(1) = b(1)/U(1,1);

    for i=2:1:n
        somma = 0;
        for k=i-1:-1:1
            somma=somma+U(i,k) * x(k);
        end

        x(i) = (b(i) - somma)/ U(i,i);
    end
end