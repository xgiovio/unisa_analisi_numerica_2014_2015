function [ values, err, A ] = eigenvalues_qr( A, ite, toll )

i=0;
err = inf;
while (i < ite) && (err > toll)
    [q,r] = qr(A);
    A1= r * q;
    err = norm(diag(A1) - diag(A),inf) / norm (diag(A1),inf);
    A = A1;
    i=i+1;
end
values = diag(A);
