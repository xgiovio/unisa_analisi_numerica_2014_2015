
% questo algoritmo converte una matrice quadrata generica in
% triangolare superiore mediante metodo di gauss con pivot parziale
% i moltiplicatori sono salvati nella matrice inferiore

%U=[10,20,30;1,5,3;4,56,34];  matrice di input
%b=[1;2;17]; termini noti

function [U,b,pivotp] = convert_matrix_to_triangular_matrix_gauss_pivoting (U,b)


    n= length(b); % lunghezza del vettore dei termini noti o numero equazioni
    norma = norm(U,inf);
    U= [U,b]; % uniamo la matrice con la colonna dei termini noti
    pivot = 1:1:n;
    pivot = pivot';

    % convertiamo la matrice 
    for i=1:1:n-1
        x_max = max ( abs(U(i:n,i)) );

        if x_max <= eps * norma
            disp('WARNING: Elemento prossimo allo zero sulla diagonale')
        end
        
        if x_max == 0
            break
        else
            
            %trovo gli indici di tutti i numeri uguali a max
            [x,y]= ind2sub(size(U),  find (abs(U(i:n,i)) == x_max) );
            x=x(1);%prendo solo il primo indice
            y=y(1);%prendo solo il primo indice
            x= x + i -1;
            y = i;

           if x~= i
                U([i x],:) = U([x i],:);
                pivot([i x]) = pivot([x i]);
                


           end

           for j=i+1:1:n

                        m= U(j,i) / U(i,i);
                        U(j,i+1:n+1) =  U(j,i+1:n+1) + ( U(i,i+1:n+1) * (- m ) ) ; 
                        U(j,i)=m;

           end
        end
    end
    
    if x_max == 0 || U(n,n) == 0
        disp('errore')
        b=[];
        U=[];
        pivot = [];
    else
        if abs(U(n,n)) <= eps * norma
            disp('WARNING: Elemento prossimo allo zero sulla diagonale')
        end
        b=U(:,n+1);
        U=U(:,1:n);
        
        pivotp=zeros(n);
        theone= diag(diag(ones(n)));
        for i=1:n
            move = pivot(i);
            pivotp(i,:)= theone(move,:);
        end
    end
   
    
end
