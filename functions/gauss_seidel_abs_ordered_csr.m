
% gauss seidel algorithm

%input
%elementi,colonne,indice_riga=ordered csr
%b=[1;2;17] termini noti
%x0 vettore di partenza
%toll tolleranza assoluta , esempio 1e-4
%nmax numero massimo di passaggi, esempio 500


%Ooutput
%x0 soluzione computata
%err errore assoluto computato
%niter numero di interazioni eseguite
%ier 1 se il numero di iterazioni ha raggiunto nmax, 0 altrimenti

function [x0,err,niter,ier] = gauss_seidel_abs_ordered_csr (elementi,colonne,indice_riga,b,x0,toll,nmax)

    n= length(b); % lunghezza del vettore dei termini noti o numero equazioni
    niter = 0;
    ier = 0;
    err = inf;% err infinito per superare subito toll
    counter = 0;
    
    while ( niter < nmax ) && ( err >= toll)
         x0_old = x0;
        for i=1:n
            partial_sum = 0;
            partial_sum2 = 0;

            start = indice_riga(i);
            stop = start;
            while colonne(stop)~= i
                stop = stop +1;
            end
            for k=start:(stop -1)
                partial_sum = partial_sum + (elementi(k) * x0(colonne(k)));
                counter = counter + 1;
            end
            for k=(stop +1):(indice_riga(i+1) - 1)
                partial_sum2 = partial_sum2 + (elementi(k) * x0(colonne(k)));
                counter = counter + 1;
                
            end
            x0(i) = (b(i) - partial_sum - partial_sum2)/ elementi(stop) ;
        end
        niter = niter + 1;
        err = norm(x0_old - x0,inf);
        mex =  [' Iterazione ', num2str(niter),' : ',mat2str(x0), ' Errore assoluto : ',  num2str(err)];
        disp (mex)
    end
    if niter == nmax
        disp('Warning: Massimo numero di step raggiunti')
        ier = 1;
    end
    counter
 
end
