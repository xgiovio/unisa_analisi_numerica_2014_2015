U=[3,0.1,2;0.3,4,-1;3.3,4.1,1]
b=[5.1;3.3;8.4]



%verifica singoalarita
if det(U) ~= 0
    disp('non singolare')
else
    disp('singolare')
end



if det(U) ~= 0
    disp('ammette singola soluzione')
else
    disp('nessuna o infinite soluzioni')
end

%verifica condizionamento
condition= cond(U)


%applico gauss naif
%[x1,y1]=convert_matrix_to_triangular_matrix_gauss_naif(U,b)
%s1=[linear_system_resolver_triangular_matrix(x1,y1)]


%applico gauss parziale
[x2,y2]=convert_matrix_to_triangular_matrix_gauss_pivoting(U,b)
s2=[linear_system_resolver_triangular_matrix(x2,y2)]


    
    

