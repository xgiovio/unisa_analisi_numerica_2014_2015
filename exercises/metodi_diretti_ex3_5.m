U=[2,5,4;0,0,10;8,2,20]
b=[11;10;30]

exact=[1,1,1];

%verifica singoalarita
if det(U) ~= 0
    disp('non singolare')
else
    disp('singolare')
end



if det(U) ~= 0
    disp('ammette singola soluzione')
else
    disp('nessuna o infinite soluzioni')
end

%verifica condizionamento
cond(U)


%applico gauss naif
convert_matrix_to_triangular_matrix_gauss_naif(U,b)


%applico gauss parziale
[x,y]=convert_matrix_to_triangular_matrix_gauss_pivoting(U,b)
x1=[linear_system_resolver_triangular_matrix(x,y)]

% verifico errore tra x calcolata e esatta

norm(x1-exact,inf)/norm(exact,inf)
    
    

