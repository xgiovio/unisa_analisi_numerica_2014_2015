U=[1.2,2.1,4.5;1.1,1.4,2.7;0.11,0.7,1.8]
b=[7.8;5.2;2.61]

exact=[1,1,1];

%verifica singoalarita
if det(U) ~= 0
    disp('non singolare')
else
    disp('singolare')
end


% 3 equazioni, 3 incognite
if det(U) ~= 0
    disp('ammette singola soluzione')
else
    disp('nessuna o infinite soluzioni')
end

%verifica condizionamento
condition= cond(U)



%applico gauss naif
[x1,y1]=convert_matrix_to_triangular_matrix_gauss_naif(U,b)
s1=[linear_system_resolver_triangular_matrix(x1,y1)]


%applico gauss parziale
[x2,y2]=convert_matrix_to_triangular_matrix_gauss_pivoting(U,b)
s2=[linear_system_resolver_triangular_matrix(x2,y2)]



% verifico errore tra soluzione calcolata e esatta
error_s1= norm(s1-exact,inf)/norm(exact,inf)
error_s2= norm(s2-exact,inf)/norm(exact,inf)


    
    

